#!/bin/sh
# shellcheck disable=SC2211
DRAWIO="$HOME/Applications/draw.io*.AppImage"
which draw.io > /dev/null 2> /dev/null && DRAWIO="draw.io"

$DRAWIO -x -o spaceinvaders.svg spaceinvaders.drawio
inkscape spaceinvaders.svg --export-type=eps -o spaceinvaders.eps --export-ignore-filters --export-ps-level=3
pandoc spec.md -o spec.pdf
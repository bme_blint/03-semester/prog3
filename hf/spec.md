# Játékleírás:

A játékot a jól ismert *Space Invaders* géptermi játékról mintázom.   
A felhasználó egy egyszerű, *retro stílusú* menüben tud:

* új játékot kezdeni
* folytatni meglévő játékot
* megnézni a dicsőségtáblát
* kilépni

Miután betöltöttük az (új) játékot, megjelenik a pálya, és az űrhajónk. 
Feladatunk a pályán érkező űrlényektől megvédeni a Földet. 
A játékmenet teltével egyre erőssebb és nagyobb létszámú ellenféllel kell majd szembenéznünk.
Bizonyos időközönként (vagy pontszám függvényében) úgynevezett *Boss*okkal kell majd megküzdenünk.
Nehezítésnek szeretnék beletenni aszteroidákat, melyeket szintén ki kell majd lőni, vagy opcionálisan kikerülhetőek.
Ha nekik megyünk akkor sérülünk, vagy meghalunk.

![Space Invaders 2020](spaceinvaders.eps)

\newpage
# Fejlesztés:  

A játékot `javaFx` `Canvas` osztályának segítségével szeretném megvalósítani.  

## Tervezett osztályok:  

| Classes  | short descriptions                                     |
| -------- |:------------------------------------------------------ |
| view.Menu     | felel a menü működéséért                               |
| model.Field    | eltárolja az űrlények és az aszteroidák helyzetét      |
| model.Alien    | űrlény, célja a *Föld* elpusztítása                    |
| model.Player   | a játékos, célja elpusztítani az űrlényeket            |
| model.Asteroid | aszterodia                                             |
| MenuView | a menü megjelenítéséért felel                          |
| FieldView| a pálya megjelenítéséért felel                         |
| view.GameView | irányításért, és az egész játék megjelenítéséért felel |

A fontosabb osztályokról részletesebben:

### model.Field:

A *field*, azaz pálya tárolja a pályán lévő entitások pozícióját, és státuszát.
Egy kényelmes adatstruktúrában tárolja el ezeket az adatokat, így a későbbi fájlbamentés folyamatát is megkönnyíti.
Az adatstruktúra a következő:
```json
{
  "player": {
    "name": "john doe",
    "health": 3,
    "position" : {
        "x" : 123,
        "y" : 456
    },
    "level" : 1,
    "bullet type" : "basic"
  },
  "aliens": [
     {
       "health" : 3,
       "position" : {
         "x" : 654,
         "y" : 321
       },
       "type" : "basic"
     }
  ],
  "asteroids": [
     {
       "health" : 3,
       "position" : {
         "x" : 654,
         "y" : 321
       },
       "type" : "basic"
     }
  ]
}
```

### Entities: 

Az *entitás* osztályok, azaz az `alien`, `asteroid` és `player` osztályok a különböző entitások adatait, valamint logikáját tárolják.
Ilyen adatok egyébként a `model.Field` osztályleírásában látható mezők, melyekben szerepelenek 
az entitás tulajdonságai, szintje és pozíciója, valamint pár entitásonként változó extra mező.

### Views:  

A *view.View* osztályok a nevükben szereplő osztályok megjelenítéséért felelősek.

A projektet verziókövetem [gitlabon](https://gitlab.com/bme_blint/03-semester/prog3/-/tree/master/hf).

---
title: Space Invaders 2020
subtitle: Programozás 3. java nagyházi
author: Réthelyi Bálint (IZUM0B)
geometry: margin=2cm
lang: hu-HU
output: pdf_document
---

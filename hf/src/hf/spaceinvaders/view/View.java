package hf.spaceinvaders.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class View extends JFrame {

    private final Menu menu = new Menu();
    private InitStart initStart = new InitStart();
    private ScoreView scoreView = new ScoreView(menu.getList());
    private GameView game;

    /**
     * létrehoz a megadott paraméterekkel egy frame-et, erre fogunk rajzolni
     * beállítja, a default paramétereket, színt, etc.
     * meghívja a menüt
     * @param x
     * @param y
     */
    public View(int x, int y) {
        super("Space Invaders");
        this.setFocusable(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(x, y);
        this.setBackground(Color.black);

        menu();
    }

    private class Listener implements MouseListener {

        /**
         * kattintásra a megfelelő függvényeket hívja meg
         * @param mouseEvent
         */
        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            if (mouseEvent.getSource() == menu.getStartButton()){
                pre_start();
            }
            if (mouseEvent.getSource() == initStart.getPlayButton()){
                start();
            }
            
            if (mouseEvent.getSource() == menu.getScoreButton()){
                score();
            }
            
            if (mouseEvent.getSource() == scoreView.getBackToMenu() ||
            mouseEvent.getSource() == initStart.getBackButton()){
                menu();
            }
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {}

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {}

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {}

        @Override
        public void mouseExited(MouseEvent mouseEvent) {}
    }

    /**
     * beállítja a menüt az aktuális panelnek
     * beállítja a gombok kattintásfigyelőit
     * validál és újrarajzol
     */
    private void menu(){
        this.setContentPane(menu);
        this.menu.getStartButton().addMouseListener(new Listener());
        this.menu.getScoreButton().addMouseListener(new Listener());
        this.validate();
        this.repaint();
    }

    /**
     * aktuális panelnek egy új init-start panelt hoz létre
     * beállítja a gombok kattintásfigyelőit
     * validál és újrarajzol
     */
    private void pre_start(){
        this.setContentPane(initStart = new InitStart());
        this.initStart.getPlayButton().addMouseListener(new Listener());
        this.initStart.getBackButton().addMouseListener(new Listener());
        this.validate();
        this.repaint();
    }

    /**
     * menti  a beírt felhasználói nevet
     * aktuális panelnek egy új game-et hoz létre
     * a fókuszt is átadja neki, így innentől a felhasználói inputokat ő kezeli
     * validál és újrarajzol
     */
    private void start(){
        String uname = initStart.getTextField();
        game = new GameView(uname, new backButton());
        this.setContentPane(game);
        game.requestFocusInWindow();
        this.validate();
        this.repaint();
    }

    /**
     * újra beolvassa a dicsőségtáblát, hátha megváltozott menet közben
     * aktuális panelnek egy új score-view-t hoz létre 
     * beállítja a gombok kattintásfigyelőit
     * validál és újrarajzol
     */
    private void score(){
        this.menu.reReadFromFile();
        this.setContentPane(scoreView = new ScoreView(menu.getList()));
        this.scoreView.getBackToMenu().addMouseListener(new Listener());
        this.validate();
        this.repaint();
    }

    /**
     * a játékra hív egy biztonsági stop-ot
     * a játékot megszünteti, így a GC el tudja takarítani
     * aktuális panelnek a menüt állítja be
     * validál és újrarajzol
     */
    private void back(){
        game.stop();
        game = null;
        this.setContentPane(menu);
        this.validate();
        this.repaint();
    }

    /**
     * meghívja a back függvényt
     */
    private class backButton implements Runnable{
        @Override
        public void run() {
            back();
        }
    }
}

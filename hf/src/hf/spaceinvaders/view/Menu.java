package hf.spaceinvaders.view;

import hf.spaceinvaders.structures.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Menu extends JPanel {
    private JButton startButton;
    private JButton scoreButton;
    private JButton quitButton;
    private ArrayList<Data> list = new ArrayList<>();


    /**
     * meghívja a fájlbeolvasást
     * és a createMenu-t
     */
    public Menu(){
        readFromFile();
        createMenu();
    }

    /**
     * getterek
     */
    public JButton getStartButton(){
        return startButton;
    }
    public JButton getScoreButton(){return scoreButton;}
    public ArrayList<Data> getList(){return list;}

    /**
     * meghívja újra a beolvasó függvényt
     */
    public void reReadFromFile(){readFromFile();}

    /**
     * létrehozza a menüt
     * beállítja a hátteret, layout-ot
     * 
     * létrehozza a címet tároló cimkét
     * gombokat hoz létre
     * 
     * beállítja a betűtípust
     * 
     * megfelelő kitöltéssel hozzáadja a panelhez őket
     */
    private void createMenu(){
        setBackground(Color.black);
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        JLabel title = new JLabel("Space Invaders");
        title.setAlignmentX(Component.CENTER_ALIGNMENT);
        title.setBackground(Color.black);
        title.setForeground(Color.white);

        startButton = new JButton("start");
        startButton.setBackground(Color.black);
        startButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        startButton.setForeground(Color.white);

        scoreButton = new JButton("scoreboard");
        scoreButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        scoreButton.setBackground(Color.black);
        scoreButton.setForeground(Color.white);

        quitButton = new JButton("quit");
        quitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        quitButton.setBackground(Color.black);
        quitButton.setForeground(Color.white);
        quitButton.addActionListener(new quitApp());

        try {

            title.setFont(MyFont.getFont(36f));
            Font font = MyFont.getFont(12f);

            startButton.setFont(font);
            scoreButton.setFont(font);
            quitButton.setFont(font);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        add(Box.createVerticalGlue());
        add(title);
        add(Box.createVerticalGlue());

        add(startButton);

        add(Box.createVerticalGlue());
        add(scoreButton);

        add(Box.createVerticalGlue());
        add(quitButton);

        add(Box.createVerticalGlue());
        add(Box.createVerticalGlue());
        add(Box.createVerticalGlue());
    }

    /**
     * beolvassa a scoreboard.txt tartalmát
     * és egy listához adja, melyet mindig újonnan hoz létre, a duplikáció elkerülése végett
     */
    private void readFromFile(){
        list = null;
        list = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File("scoreboard.txt"));
            while (scanner.hasNextLine()){
                String[] parts = scanner.nextLine().split("\\|");
                
                list.add(new Data(parts[0], Integer.parseInt(parts[1])));
            }
            scanner.close();
        } catch (IOException e){
            e.printStackTrace();
        }
        
        
    }

    private class quitApp implements ActionListener{

        /**
         * bezárja az alkalmazást
         * @param actionEvent
         */
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            System.exit(0);
        }
    }
}

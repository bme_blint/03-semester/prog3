package hf.spaceinvaders.view;

import java.awt.*;

public class MyFont {
    private static Font font;

    /**
     * ha nincs betöltve a fontunk, betölti,
     * ha nem található, hibát dob
     */
    public static void init(){
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, MyFont.class.getResource("/font.ttf").openStream());
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * ha a font nem létezik, betölti azt
     * visszaad egy paraméterként kapott betűméretú fontot
     * @param size
     * @return
     */
    public static Font getFont(float size) {
        if (font == null) {
            init();
        }
        return font.deriveFont(size);
    }
}

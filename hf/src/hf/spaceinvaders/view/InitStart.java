package hf.spaceinvaders.view;

import javax.swing.*;
import java.awt.*;

public class InitStart extends JPanel {
    private final JLabel label;
    private final JTextField textField;
    private final JButton playButton;
    private JButton backButton;

    /**
     * beállítja a hátteret, layoutot
     * címkét állít be
     * szövegmezőt állít be
     * felveszi a gombokat
     * beállítja a fontot, méretet
     * majd hozzáadja a panelhez
     */
    public InitStart() {
        setBackground(Color.black);

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        label = new JLabel("Your name");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBackground(Color.black);
        label.setForeground(Color.white);

        textField = new JTextField();
        textField.setAlignmentX(Component.CENTER_ALIGNMENT);
        textField.setBackground(Color.black);
        textField.setForeground(Color.white);
        textField.setMaximumSize(new Dimension(400, 100));

        playButton = new JButton("play");
        playButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        playButton.setBackground(Color.black);
        playButton.setForeground(Color.white);

        backButton = new JButton("back");
        backButton.setBackground(Color.black);
        backButton.setForeground(Color.white);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        try {

            Font font = MyFont.getFont(12f);

            playButton.setFont(font);
            backButton.setFont(font);
            label.setFont(font);
            textField.setFont(font);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        add(Box.createVerticalGlue());
        add(label);
        add(Box.createVerticalGlue());

        add(textField);

        add(Box.createVerticalGlue());
        add(playButton);

        add(Box.createVerticalGlue());
        add(backButton);

        add(Box.createVerticalGlue());
        add(Box.createVerticalGlue());
        add(Box.createVerticalGlue());
    }

    /**
     * getterek
     */
    public JButton getPlayButton(){return playButton;}
    public JButton getBackButton(){return backButton;}
    public String getTextField(){return textField.getText();}
    
}

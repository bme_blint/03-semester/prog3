package hf.spaceinvaders.view;

import hf.spaceinvaders.structures.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;

public class ScoreView extends JPanel {
    private JButton backToMenu;

    /**
     * létrehoz egy saját modellt
     * a modell alapján egy táblázatot
     * a modellhez hozzáadja a paraméterként átvett listából a sorokat
     * beállítja a táblázat színét, betűszínét, átlátszóságát
     * beállítja, hogy automatikusan lehessen sorbarendezni az értékeket
     *
     * a táblázatot egy görgethető elemre illeszti
     * 
     * betűtípust és méretet állít
     * @param list
     */
    public ScoreView(ArrayList<Data> list) {

        Model model = new Model();
        JTable table = new JTable(model);

        for (int i = 0; i < list.size(); ++i){
            model.addRow(new Object[] {list.get(i).getName(), list.get(i).getScore()});
        }

        table.setBackground(Color.black);
        table.setForeground(Color.white);
        table.setOpaque(true);


        table.setAutoCreateRowSorter(true);

        table.getTableHeader().setBackground(Color.black);
        table.getTableHeader().setForeground(Color.white);

        RowSorter sorter = table.getRowSorter();
        ArrayList<RowSorter.SortKey> sortList = new ArrayList<>();
        sortList.add(new RowSorter.SortKey(1, SortOrder.DESCENDING));
        sorter.setSortKeys(sortList);

        JScrollPane scroll = new JScrollPane(table);
        scroll.setOpaque(true);
        scroll.setBackground(Color.black);
        scroll.getViewport().setBackground(Color.black);


        backToMenu = new JButton("back");
        backToMenu.setBackground(Color.black);
        backToMenu.setForeground(Color.white);
        backToMenu.setAlignmentX(Component.CENTER_ALIGNMENT);

        try {

            Font font = MyFont.getFont(12f);

            backToMenu.setFont(font);
            table.setFont(font);
            table.getTableHeader().setFont(font);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        table.setBackground(Color.black);
        table.setForeground(Color.white);

        setBackground(Color.black);
        setForeground(Color.white);

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        add(scroll);
        add(backToMenu);
    }
    
    private class Model extends DefaultTableModel {

        /**
         * beállítja az oszlopokat
         * @param columnIndex
         * @return
         */
        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return String.class;
                case 1:
                    return Integer.class;
            }
            return super.getColumnClass(columnIndex);
        }

        /**
         * egy sort ad hozzá a modellhez
         * @param rowData
         */
        @Override
        public void addRow(Object[] rowData) {
            super.addRow(rowData);
        }
    }

    /**
     * @return - getter
     */
    public JButton getBackToMenu(){return backToMenu;}
}

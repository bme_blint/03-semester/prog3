package hf.spaceinvaders.view;

import hf.spaceinvaders.model.Field;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GameView extends JPanel implements ActionListener {
    private final Field field;
    private final Timer timer = new Timer(1000/60, this);

    /**
     * továbbadja a paraméterként kapott értékeket
     * beállítja a panelt, létrehoz egy új action figyelőt
     * elindítja a timer-t
     * @param name
     * @param back
     */
    public GameView(String name, Runnable back){
        field = new Field(name, back, this);
        setFocusable(true);
        addKeyListener(new listener());
        timer.start();
    }

    /**
     * leállítja a field-et
     * leállítja a timer-t
     */
    public void stop(){
        field.stop();
        timer.stop();
    }

    /**
     * beállítja a hátteret, átlátszóságot
     * kirajzolja a field-et
     * majd meghívja a paintComponent-et, ezzel rajzolva
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        setBackground(Color.BLACK);
        setOpaque(false);
        field.draw(g);
        super.paintComponent(g);
    }

    /**
     * újra rajzol minden alkalommal
     * @param actionEvent
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        repaint();
    }
    
    public class listener implements KeyListener{

        /**
         * ha a billentyű le volt nyomva, true-ra állítja az
         * újonnan létrehozott InputEvent pressed értékét
         * majd meghívja a field-et
         * @param keyEvent
         */
        @Override
        public void keyPressed(KeyEvent keyEvent) {
            InputEvent event = new InputEvent(keyEvent, true);
            field.inputEvent(event);
        }

        /**
         * ha a billentyű fel lett engedve, false-ra állítja az
         * újonnan létrehozott InputEvent pressed értékét
         * majd meghívja a field-et
         * @param keyEvent
         */
        @Override
        public void keyReleased(KeyEvent keyEvent) {
            InputEvent event = new InputEvent(keyEvent, false);
            field.inputEvent(event);
        }

        @Override
        public void keyTyped(KeyEvent keyEvent) {}
    }
}
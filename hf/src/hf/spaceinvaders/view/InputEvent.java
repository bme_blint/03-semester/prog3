package hf.spaceinvaders.view;

import java.awt.event.KeyEvent;

public class InputEvent {
    private final KeyEvent event;
    private final boolean pressed;

    /**
     * beállítja a paraméterként kapott étékeket
     * @param e
     * @param p
     */
    public InputEvent(KeyEvent e, boolean p) {
        event = e;
        pressed = p;
    }

    /**
     * getter, setter
     */
    public KeyEvent getEvent(){return event;}
    
    public boolean getPressed(){return pressed;}
}

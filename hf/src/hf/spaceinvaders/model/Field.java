package hf.spaceinvaders.model;

import hf.spaceinvaders.view.InputEvent;
import hf.spaceinvaders.view.MyFont;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Field extends Sprite {
    private final ArrayList<Sprite> sprites;
    private final Alien templateAlien = new Alien(-1, -1, 3, 1);
    private boolean gameOver = false;
    private int tickCount = 0;
    private int rowsToCreate = 2;
    private Thread thread = new Thread(new ticker());
    private final JButton backButton;
    private final Runnable back;

    /**
     * eltárolja a paraméterként kapott back futtatható változót
     * létrehozza a sprites listát
     * létrehozza a játékost és hozzáadja a listához
     * létrehoz egy vissza gombot és beállítja tulajdonságait,
     * legfontosabb, hogy kikapcsolja a láthatóságát
     * majd elindít egy thread-et
     * @param playerName
     * @param bck
     * @param p
     */
    public Field(String playerName, Runnable bck, JPanel p){
        super(-1,-1);

        back = bck;

        sprites = new ArrayList<>();
        sprites.add(new Player(playerName));

        backButton = new JButton("back");
        backButton.setBackground(Color.black);
        backButton.setForeground(Color.white);
        backButton.addMouseListener(new Back());
        backButton.setVisible(false);
        p.add(backButton);

        thread.start();
    }

    /**
     * meghívja az összes sprite draw függvényét
     * ha gameOver van, kiírja, valamint láthatóvá teszi a vissza gombot
     * @param graphics
     */
    @Override
    public void draw(Graphics graphics){

        for (int i = 0; i < sprites.size(); ++i){
            sprites.get(i).draw(graphics);
        }
        
        if (gameOver){

            String gameOverMessage = "Game Over";
            FontMetrics metrics = graphics.getFontMetrics(MyFont.getFont(48f));
            int x = (800 - metrics.stringWidth(gameOverMessage)) / 2;
            int y = (600 - metrics.getHeight()) / 2;
            graphics.setFont(MyFont.getFont(48f));
            graphics.setColor(Color.white);
            graphics.drawString(gameOverMessage, x, y);

            if (!backButton.isVisible()){
                backButton.setVisible(true);
            }
        }
    }

    private class Back implements MouseListener {
        
        /**
         * a kattintást figyeli, ha a visszagombra kattintott a felhasználó,
         * meghívja visszalép
         * @param mouseEvent
         */
        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            if (mouseEvent.getSource() == backButton){
                back.run();
            }
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {}

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {}

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {}

        @Override
        public void mouseExited(MouseEvent mouseEvent) {}
    }

    /**
     * a gameOver változó értékét beállítja igazra
     * biztonsági függvény, ha nem lenne beállítva korábban
     */
    public void stop(){
        gameOver = true;
    }

    /**
     * minden sprite inputEvent-et hív
     * @param event - saját event osztályt kér be
     */
    @Override
    public void inputEvent(InputEvent event) {

        for (int i = 0; i < sprites.size(); ++i){
            sprites.get(i).inputEvent(event);
        }
    }

    /**
     * kezeli, ha vége a játéknak
     */
    private class gameOverHandler implements Runnable {
        @Override
        public void run() {
            gameOver = true;
        }
    }
    
    private final gameOverHandler gameOverHandler = new gameOverHandler();

    /**
     * kirajzol n sornyi, hp életerejű, lvl szintű alient
     * @param n
     * @param hp
     * @param lvl
     */
    private void createRows(int n, int hp, int lvl){
        for (int i = 0; i < 800/50; ++i){
            for (int j = 0; j < n; ++j){
                sprites.add(new Alien((i * templateAlien.getSizeX()), j * templateAlien.getSizeY(), hp, lvl));
            }
        }
    }

    /**
     * megfelelő időnként
     * kirajzol új sorokat,
     * növeli az alien-ek életerejét, vagy szintjét
     * növeli, hogy hány sort rajzoljon legközelebb,
     * az alienek sebességét is növeli
     * valamint a tick-számláló értékét is
     * @param sprites
     * @param gameOver
     */
    @Override
    public void tick(List<Sprite> sprites, Runnable gameOver) {
        if ((tickCount % (120 * 20)) == 0){
            createRows(rowsToCreate, templateAlien.getHealth(), templateAlien.getLevel());
        }
        
        if ((tickCount % (120 * 60)) == 0){
            Random rand = new Random();
            switch (rand.nextInt(2)) {
                case 0:
                    templateAlien.setHealth(templateAlien.getHealth() + 1);
                    break;
                case 1:
                    if (templateAlien.getLevel() >= 4){
                        templateAlien.setLevel(templateAlien.getLevel() + 1);
                    } else {
                        templateAlien.setHealth(templateAlien.getHealth() + 1);
                    }
                    break;
            }
        }
        
        if ((tickCount % (120 * 120) == 0)){
            rowsToCreate++;
        }
        
        if ((tickCount % (120 * 120) == 0)){
            if (Alien.getTickSlower() > 5){
                Alien.setTickSlower(Alien.getTickSlower() - 5);
            } else {
                Alien.setStatVelocity(Alien.getStatVelocity() + 5);
            }
        }
        
        tickCount++;
    }
    
    private class ticker implements Runnable {
        
        /**
         * a thread elindítja
         * addig fut, míg gameOver nem igaz
         * minden sprite tick-jét meghívja
         * meghívja a saját tick-jét is
         * majd vár, hogy 120 fps-ünk legyen
         */
        @Override
        public void run() {
            while (!gameOver) {

                for (int i = 0; i < sprites.size(); ++i){
                    sprites.get(i).tick(sprites, gameOverHandler);
                }

                tick(sprites, gameOverHandler);

                try {
                    Thread.sleep(1000/120);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    /** getters for testing **/
    /** autogenerated with intellij **/

    public ArrayList<Sprite> getSprites() {
        return sprites;
    }

    public Alien getTemplateAlien() {
        return templateAlien;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public int getTickCount() {
        return tickCount;
    }

    public int getRowsToCreate() {
        return rowsToCreate;
    }

    public Thread getThread() {
        return thread;
    }

    public JButton getBackButton() {
        return backButton;
    }

    public Runnable getBack() {
        return back;
    }

    public Field.gameOverHandler getGameOverHandler() {
        return gameOverHandler;
    }
}
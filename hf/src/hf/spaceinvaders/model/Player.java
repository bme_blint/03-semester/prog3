package hf.spaceinvaders.model;

import hf.spaceinvaders.view.*;
import hf.spaceinvaders.structures.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Player extends Sprite {
    private int bulletLevel;
    public ArrayList<Bullet> bullets;
    private int direction = 0;
    private boolean isShooting = false;
    private String name;
    private int score = 0;
    private int tickCount = 0;
    private ArrayList<Heart> hearts;
    private long lastCollision = 0;
    private boolean saved = false;

    /**
     * létrehoz egy játékost
     * beállítja a pozícióját a Sprite ősön keresztül
     * beállítja az alap tulajdonságokat
     * inicializálja a listákat
     * betölti a képet
     * létrehozza az életerőt mutató képeket is
     * @param playerName - beállítja a játékos nevét
     */
    public Player(String playerName) {
        super(400-(double)40/2, 600-40-20);
        health = 3;
        level = 1;
        bulletLevel = 1;
        bullets = new ArrayList<>();
        hearts = new ArrayList<>();
        velocity = 2;
        name = playerName;
        sizeX = 40;
        sizeY = 40;

        String filename = "spaceship2.png";

        loadImage("/images/" + filename);
        
        int heartX = (new Heart(-10, 0)).sizeX;
        
        for (int i = 1; i <= health; ++i){
            hearts.add(new Heart(800 - heartX * i, 0));
        }
    }

    public class Bullet extends Sprite {
        
        /**
         * létrehoz egy golyót
         * ős segítségével beállítja a pozíciót
         * beállítja az alap tulajdonságokat
         * betölti a képet
         */
        public Bullet() {
            super(getPlayerPosition().getX() + (double)40/2 - (double)3/2, getPlayerPosition().getY() - 15);
            level = getBulletLevel();
            velocity = 2;
            sizeX = 3;
            sizeY = 15;
            health = 1;

            loadImage("/images/bullet.png");
        }

        /**
         * beállítja a paraméterként kapott szintet a golyó szintjének
         * @param l
         */
        public void setLevel(int l){
            level = l;
        }

        /**
         * @return - visszaadja a szintet
         */
        public int getLevel() {
            return level;
        }

        /**
         * a golyó tick-je, ez fut le minden timer-híváskor
         * beállítja a pozícióját a golyónak
         * ha kimenne a pályáról, akkor törli azt
         * megnézi az összes sprite-ra, hogy ütköznek-e
         * megnézi, hogy meg kell-e halljon, ha igen, törli magát
         * @param sprites - Sprite-ok listája
         * @param gameOver - az override miatt van rá szükség, itt nincs kihasználva
         */
        @Override
        public void tick(List<Sprite> sprites, Runnable gameOver) {
                       
            position.setY(position.getY() - velocity);
            if (position.getY() < -15){
                delBullet(this);
            }

            sprites.forEach(this::checkCollision);
            
            if (shouldDie()){
                delBullet(this);
            }
            
        }

        /**
         * ha a sprite nem önmaga,
         * akkor ellenőrzi, hogy ötköztek-e
         * ha igen, az ütközött sprite életerejét csökkenti a saját szintjével
         * ha a sprite életereje 0, azaz meghalt, akkor a sprite szintjének 10x-ével növeli a játékos pontját
         * majd a saját életereje -1-re állításával megsemmisíti magát
         * @param sprite - az adott sprite, akivel való ütközést vizsgálja
         */
        @Override
        protected void checkCollision(Sprite sprite) {
            if (sprite != this){
                if (position.getX() < sprite.getPosition().getX() + sprite.sizeX && position.getX() + sizeX > sprite.getPosition().getX() &&
                        position.getY() < sprite.getPosition().getY() + sprite.sizeY && position.getY() + sizeY > sprite.getPosition().getY()){
                    sprite.setHealth(sprite.getHealth() - this.level);
                    
                    if (sprite.getHealth() == 0)
                        setScore(getScore() + sprite.getLevel() * 10);
                    this.health = -1;
                }
            }
        }
    }
    
    public static class Heart extends Sprite{

        /**
         * beállítja az ős segítségével a pozíciót
         * majd betölti a képet
         * @param x - x koordináta
         * @param y - y koordináta
         */
        
        public Heart(double x, double y) {
            super(x, y);

            sizeX = 35;
            sizeY = 35;

            loadImage("/images/heart.png");
        }
    }

    /**
     * létrehoz egy golyót
     * majd eltárolja
     */
    private void shoot(){
        Bullet bullet = new Bullet();
        bullets.add(bullet);
    }

    /**
     * törli a paraméterként kapott golyót
     * @param bullet - törlendő golyó
     */
    public void delBullet(Bullet bullet){
        bullets.remove(bullet);
    }

    /**
     * @return - visszaadja, hogy a játékos milyen erős golyókat használ
     */
    public int getBulletLevel(){
        return bulletLevel;
    }

    /**
     * beállítja, hogy a játékos milyen erős golyókat használjon
     * nem használt függvény, későbbi bővíthetőség miatt létezik
     * @param l
     */
    private void setBulletLevel(int l){
        bulletLevel = l;
    }

    /**
     * visszaadja a játékos pozícióját
     * szükséges a golyók létrehozásához, nem elég az ős függvénye
     * @return - játékos pozíciója
     */
    private Position getPlayerPosition() {
        return position;
    }

    /**
     * @return - a játékos pontjával tér vissza
     */
    public int getScore(){return score;}

    /**
     * beállítja a játékos pontját a paraméterként kapott értékre
     * @param score - beállítandó érték
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * ellenőrzi a játékost, hogy ne tudjon kifutni a pályáról
     */
    private void guard(){
        //left guard
        if (position.getX() < 0){
            position.setX(1);
        }

        //right guard
        if (position.getX() > 800 - 40){
            position.setX(800 - 40 - 1);
        }
    }

    /**
     * a játékos pozícióját állítja, az irány és a sebesség függvényében
     */
    private void move(){position.setX(position.getX() + direction * velocity);}

    /**
     * megfelelő időközönként növeli a játékos pontját
     */
    private void scoreUpdater(){
        if ((tickCount % 100) == 0){
            score++;
        }
    }

    /**
     * fájlba menti a játékos nevét és pontszámát
     */
    private void saveToFile(){
        if (!saved) {
            try {
                FileWriter file = new FileWriter("scoreboard.txt", true);
                file.write(name + "|" + score + "\n");
                file.close();
                saved = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * vizsgálja egy sprite-al való ütközést
     * ha nem önmaga, és az utolsó ütközés óta eltelt 1 sec,
     * és ütközik, csökkenti az életerejét,
     * valamint beállítja az utolsó ütközés idejét
     * @param sprite - az adott sprite, akivel való ütközést vizsgálja
     */
    @Override
    protected void checkCollision(Sprite sprite) {
        if (sprite != this && System.currentTimeMillis()-lastCollision > 1000){
            if (position.getX() < sprite.getPosition().getX() + sprite.sizeX && position.getX() + sizeX > sprite.getPosition().getX() &&
                    position.getY() < sprite.getPosition().getY() + sprite.sizeY && position.getY() + sizeY > sprite.getPosition().getY()){
                health --;
                lastCollision = System.currentTimeMillis();
            }
        }
    }

    /**
     * minden timer híváskor lefut
     * meghívja a guard és move függvényeket
     * meghívja az összes golyó tick-jét
     * vizsgálja a sprite-okal való ütközést
     * ha meg kell halnia, ment, és meghívja a paraméterként kapott gameOver-t,
     * ezzel elindítva a játék vége folyamatot
     * növeli  a pontokat, és a tick-számláló értékét
     * @param sprites
     * @param gameOver
     */
    @Override
    public void tick(List<Sprite> sprites, Runnable gameOver) {

        guard();

        move();

        for (int i = 0; i < bullets.size(); ++i){
            bullets.get(i).tick(sprites, gameOver);
        }

        sprites.forEach(this::checkCollision);
        
        if (shouldDie()){

            saveToFile();

            gameOver.run();
        }

        scoreUpdater();

        tickCount++;
        
    }

    /**
     * a panel tetején látható értékeket rajzolja ki
     * @param graphics
     */
    private void drawBar(Graphics graphics){
        String scoreString = "score : " + score;

        FontMetrics metrics = graphics.getFontMetrics(MyFont.getFont(24f));
        int y = metrics.getHeight();
        graphics.setFont(MyFont.getFont(24f));
        graphics.setColor(Color.lightGray);
        graphics.drawString(scoreString, 5, y);
    }

    /**
     * meghívja az ős rajzoló függvényét,
     * valamint az összes golyó draw-ját,
     * kirajzolja a drawBar-t,
     * valamint ellenőrzi, hogy megfelelő mennyiségű életerő van kirajzolva,
     * majd kirajzolja őket
     * @param graphics
     */
    @Override
    public void draw(Graphics graphics) {
        super.draw(graphics);

        for (int i = 0; i < bullets.size(); ++i){
            bullets.get(i).draw(graphics);
        }

        drawBar(graphics);
        
        if (health < hearts.size()){
            hearts.remove(hearts.size()-1);
        }
        
        for (int i = 0; i < hearts.size(); ++i){
            hearts.get(i).draw(graphics);
        }
    }

    /**
     * kezeli a felhasználói inputokat,
     * a, s, ballra, jobbra, space
     * @param event - saját event osztályt kér be
     */
    @Override
    public void inputEvent(InputEvent event) {
        if (event.getPressed()){
            switch (event.getEvent().getKeyCode()) {
                case 37:
                case 65:
                    direction = -1;
                    break;
                case 39:
                case 68:
                    direction = 1;
                    break;
                case 32:
                    if (!isShooting){
                        shoot();
                        isShooting = true;
                    }
            }
        } else {
            switch (event.getEvent().getKeyCode()) {
                case 37:
                case 65:
                    if (direction == -1)
                        direction = 0;
                    break;
                case 39:
                case 68:
                    if (direction == 1)
                        direction = 0;
                    break;
                case 32:
                    isShooting = false;
            }
        }
    }
    
    /** getters for testing **/
    /** autogenerated with intellij **/

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }

    public String getName() {
        return name;
    }

    public boolean isShooting() {
        return isShooting;
    }

    public int getTickCount() {
        return tickCount;
    }

    public ArrayList<Heart> getHearts() {
        return hearts;
    }

    public boolean isSaved() {
        return saved;
    }
}
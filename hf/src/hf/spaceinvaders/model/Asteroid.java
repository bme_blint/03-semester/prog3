package hf.spaceinvaders.model;

public class Asteroid extends Sprite {

    /**
     * nem használt osztály,
     * későbbi bővíthetőség céljából létezik
     * @param x
     * @param y
     * @param hp
     * @param lvl
     */
    public Asteroid(double x, double y, int hp, int lvl) {
        super(x, y);
        health = hp;
        level = lvl;
    }
}

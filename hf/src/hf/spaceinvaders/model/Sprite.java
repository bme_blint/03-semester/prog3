package hf.spaceinvaders.model;

import hf.spaceinvaders.view.*;
import hf.spaceinvaders.structures.*;
import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public abstract class Sprite {

    protected int health;
    protected int level;
    protected int sizeX;
    protected int sizeY;
    
    private static HashMap<String, Image> cache = new HashMap<>();
    
    public Position position;
    protected Image img = null;
    protected int velocity;


    /**
     * konstruktor, beállítja a pozíciót
     * @param x - x koordináta
     * @param y - y koordináta
     */
    public Sprite(double x, double y) {
        position = new Position(x, y);
    }

    /**
     * betölti a filename-ben megadott fájlt, a cacheből, így nincs folytonos olvasásunk fájlból
     * @param filename
     */
    protected void loadImage( String filename) {
        img = cache.get(filename);
        if (img == null){

            try {
                img = ImageIO.read(getClass().getResourceAsStream(filename)).getScaledInstance(sizeX, sizeY, Image.SCALE_FAST);
                cache.put(filename, img);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return - visszaadja a Sprite szintjét
     */
    public int getLevel(){
        return level;
    }

    /**
     * beállítja a paraméterként kapott szintet a Sprite szintjének
     * @param l
     */
    public void setLevel(int l){
        level = l;
    }

    /**
     * @return - visszaadja a Sprite pozícióját
     */
    public Position getPosition(){
        return position;
    }

    /**
     * @return - visszaadja a Sprite életerejét
     */
    public int getHealth(){
        return health;
    }

    /**
     * beállítja a paraméterként kapott életerőt
     * @param hp
     */
    public void setHealth(int hp){
        health = hp;
    }

    /**
     * kirajzolja a Sprite-ot
     * @param graphics
     */
    public void draw(Graphics graphics){
        graphics.drawImage(img, (int)position.getX(), (int)position.getY(), null);
    }

    /**
     * override-olható függvény, ezt fogja hívni a timer
     * @param sprites
     * @param gameOver
     */
    public void tick(List<Sprite> sprites, Runnable gameOver){}

    /**
     * kezeli a user-inputokat
     * szintén override-olható
     * @param event - saját event osztályt kér be
     */
    public void inputEvent(InputEvent event){}

    /**
     * @return - visszaadja, hogy a Sprite-nak meg kell-e hallnia
     */
    protected boolean shouldDie(){return health <= 0;}

    /**
     * override-olható függvény
     * ellenőrzi, hogy volt e ütközés
     * @param sprite - az adott sprite, akivel való ütközést vizsgálja
     */
    protected void checkCollision(Sprite sprite){}

    /**
     * @return - visszaadja a Sprite méretének X paraméterét
     */
    public int getSizeX() {
        return sizeX;
    }

    /**
     * @return - visszaadja a Sprite méretének Y paraméterét
     */
    public int getSizeY() {
        return sizeY;
    }

    /**
     * @return - visszaadja a sebességét
     */
    public int getVelocity() {
        return velocity;
    }
}
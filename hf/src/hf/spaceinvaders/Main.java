package hf.spaceinvaders;

import hf.spaceinvaders.view.View;

public class Main {

    /**
     * A main class eldönti a futtató operációs rendszer alapján a leendő frame méretét
     */
    public static void main(String[] args) {
        String os = System.getProperty("os.name");
        View view;
        if (os.contains("Windows")){
            view = new View(820, 630);
            //System.out.println("Frame is 820x630 pxs");
        } else {
            view = new View(800, 600);
            //System.out.println("Frame is 800x600 pxs");
        }
        view.setVisible(true);
    }
}

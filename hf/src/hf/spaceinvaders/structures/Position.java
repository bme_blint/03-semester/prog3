package hf.spaceinvaders.structures;

public class Position {
    double x;
    double y;

    /**
     * beállítja a koordinátákat
     * @param x_coords
     * @param y_coords
     */
    public Position(double x_coords, double y_coords){
        x = x_coords;
        y = y_coords;
    }

    /**
     * setterek, getterek
     * @return
     */
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
package hf.spaceinvaders.structures;

public class Data {
    String name;
    Integer score;

    /**
     * beállítja a paraméterként kapott adatokat
     * @param n
     * @param sc
     */
    public Data(String n, int sc) {
        name = n;
        score = sc;
    }

    /**
     * setterek, getterek
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}

package kaszino;

public abstract class Jatekos {
    protected Asztal asztal;

    public void lep() {
        System.out.println("A "+asztal.getKor()+"-ik kor, a tet: "+asztal.getTet()+"\n");
    }

    public void setAsztal(Asztal a) {
        this.asztal = a;
    }
    
    @Override
    public void finalize(){
        System.out.println(this.hashCode());
        System.out.println(this);
    }
}

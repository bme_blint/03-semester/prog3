package kaszino;

public class Main {

    public static void main(String[] args) {
        
        Asztal asztal = new Asztal();
        asztal.ujJatek();

        /*asztal.addJatekos(new Kezdo("!mike"));
        asztal.addJatekos(new Kezdo("blint"));
        asztal.addJatekos(new Robot());*/

        asztal.addJatekos(new Mester(20));
        asztal.addJatekos(new Nyuszi("piros"));
        asztal.addJatekos(new Ember());
        
        for (int i = 0; i < 10; ++i){
            try {
                asztal.kor();
            }
            catch (NincsJatekos e){
                System.out.println(e);
            }
        }
        asztal = null;
        System.gc();
    }
}

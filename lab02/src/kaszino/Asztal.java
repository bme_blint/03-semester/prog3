package kaszino;
import java.util.Random;


public class Asztal {

    Random rand = new Random();

    private Jatekos[] jatekosok;
    private double tet;
    private int kor;
    private double goal;
    private boolean jatek;
    private int hany;
    public static int hanyRobot;

    public void ujJatek() {
        tet = 0;
        kor = 0;
        goal = rand.nextDouble() * 100;
        jatekosok = new Jatekos[10];
        jatek = true;
        hany = 0;
        hanyRobot = 0;
    }
    public void addJatekos(Jatekos j){
        if (hany >= 10){
            System.out.println("Az azstal tele van\n");
        } else {
            jatekosok[hany] = j;
            j.setAsztal(this);
            hany++;
        }
    }

    public int getKor() {
        return kor;
    }

    public double getTet() {
        return tet;
    }
    
    public void emel(double d){
        tet+=d;
    }
    public void kor() throws NincsJatekos {
        if (jatek){
            if (hany > 0) {
                for (int i = 0; i < hany; ++i) {
                    if (tet < goal){
                        jatekosok[i].lep();
                    } else if (tet > goal && tet  <= goal * 1.1) {
                        System.out.println((i+1)+"-ik jatekos nyert\n");
                        jatek = false;
                        break;
                    } else {
                        System.out.println("Mindenki vesztett!\n");
                        jatek = false;
                        break;
                    }
                }
                System.out.println("A tet aktualis allasa: "+tet+"\n");
                kor++;
            } else {
                throw new NincsJatekos();
            }
        } else {
            System.out.println("Vege a jateknak\n");
        }
    }
    
}

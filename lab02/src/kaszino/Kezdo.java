package kaszino;

public class Kezdo extends Jatekos {
    private String  name;
    public Kezdo(String n) {
        super();
        name = n;
    }
    
    public String toString() {
        return name;
    }
    
    public void lep() {
        System.out.println(this);
        System.out.println(asztal.getKor());
        if (asztal.getKor() % 2 == 0) {
            asztal.emel(1);
        }
    }
}

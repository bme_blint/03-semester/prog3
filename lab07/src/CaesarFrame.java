import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class CaesarFrame extends JFrame {
    private final JTextField input;
    private final JTextField output;
    private final JComboBox comboBox1;

    private Component lastFocused;

    public CaesarFrame(String name) {
        super(name);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(400, 110);

        GridLayout layout = new GridLayout();
        layout.setRows(2);
        layout.setColumns(1);
        this.setLayout(layout);

        Object chars[]=new Object[26];
        for(int i=(int)'A';i <= (int)'Z';++i)
            chars[i-(int)'A'] = (char)i;
        JPanel panel1 = new JPanel();
        comboBox1 = new JComboBox(chars);
        input = new JTextField(20);
        lastFocused = input;
        input.addFocusListener(new MyFocusListener());
        input.setEditable(true);
        input.getDocument().addDocumentListener(new InputFieldKeyListener());
        JButton button1 = new JButton("Code!");
        button1.addActionListener(new OkButtonActionListener());
        panel1.add(comboBox1);
        panel1.add(input);
        panel1.add(button1);

        JPanel panel2 = new JPanel();
        JLabel label1 = new JLabel("Output:");
        output = new JTextField(20);
        output.addFocusListener(new MyFocusListener());
        output.setEditable(true);
        panel2.add(label1);
        panel2.add(output);

        this.add(panel1);
        this.add(panel2);
        this.setResizable(true);
    }

    public class OkButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (lastFocused == output)
                input.setText(CaesarCode.caesarDeCode(output.getText(), (char)comboBox1.getSelectedItem()));
            else
                output.setText(CaesarCode.caesarCode(input.getText(), (char)comboBox1.getSelectedItem()));
        }
    }

    public class InputFieldKeyListener implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            output.setText(CaesarCode.caesarCode(input.getText(), (char)comboBox1.getSelectedItem()));
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            output.setText(CaesarCode.caesarCode(input.getText(), (char)comboBox1.getSelectedItem()));
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            output.setText(CaesarCode.caesarCode(input.getText(), (char)comboBox1.getSelectedItem()));
        }
    }

    public class MyFocusListener implements FocusListener {

        @Override
        public void focusGained(FocusEvent focusEvent) {
            lastFocused = focusEvent.getComponent();
        }

        @Override
        public void focusLost(FocusEvent focusEvent) {

        }
    }
}
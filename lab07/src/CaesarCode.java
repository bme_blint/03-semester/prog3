public class CaesarCode {

    public static String caesarCode(String input, char offset) {
        input = input.toUpperCase();
        offset = Character.toUpperCase(offset);
        offset = (char)(offset - 'A');
        StringBuilder out = new StringBuilder();

        for (char c: input.toCharArray()) {
            c = (char)(c + offset);
            if (c > 'Z') {
                out.append(c - (26 - offset));
            } else {
                out.append(c);
            }
        }
        return out.toString();
    }

    public static String caesarDeCode(String input, char offset) {
        input = input.toUpperCase();
        offset = Character.toUpperCase(offset);
        offset = (char)(offset - 'A');
        StringBuilder out = new StringBuilder();

        for (char c: input.toCharArray()) {
            c = (char)(c - offset);
            if (c < 'A') {
                out.append(c + (26 + offset));
            } else {
                out.append(c);
            }
        }
        return out.toString();
    }
}

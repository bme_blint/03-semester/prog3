public class Main {

    public static void main(String[] args) {
        System.out.println(CaesarCode.caesarCode("alma", 'b'));
        System.out.println(CaesarCode.caesarDeCode("bmnb", 'b'));

        CaesarFrame frame = new CaesarFrame("SwingLab");
        frame.setVisible(true);
    }
}

import sample.calc.Calculator;

public class Main {
    
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 0; i < args.length; i++){
            Calculator c =new Calculator();
            sum = c.add(sum, Integer.parseInt(args[i]));
        }
        System.out.println(sum);
    }
}

package junitlab.bank;

import junitlab.bank.impl.FirstNationalBank;
import org.junit.Assert;
import org.junit.Test;

public class BankTest {
    FirstNationalBank bank = new FirstNationalBank();

    @Test
    public void testOpenAccount() throws AccountNotExistsException {
        String account = bank.openAccount();
        Assert.assertNotNull(account);
        long balance = bank.getBalance(account);
        Assert.assertEquals(balance, 0L);
    }

    @Test
    public void testUniqueAccount() {
        String acc1 = bank.openAccount();
        String acc2 = bank.openAccount();
        Assert.assertNotSame(acc1, acc2);
    }

    @Test (expected = AccountNotExistsException.class)
    public void testInvalidAccount() throws AccountNotExistsException {
        long balance = bank.getBalance("notavalidaccountnumberhehehe");
    }

    @Test
    public void testDeposit() throws AccountNotExistsException {
        String account = bank.openAccount();
        bank.deposit(account, 2000);
        long balance = bank.getBalance(account);
        Assert.assertEquals(balance, 2000);
    }
}
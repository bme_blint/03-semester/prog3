package junitlab.bank;

import junitlab.bank.impl.FirstNationalBank;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BankTestFixture {
    public FirstNationalBank bank;
    public String acc1;
    public String acc2;

    @Before
    public void init() throws AccountNotExistsException {
        bank = new FirstNationalBank();
        acc1 = bank.openAccount();
        acc2 = bank.openAccount();
        bank.deposit(acc1,  1500);
        bank.deposit(acc2, 12000);
    }

    @Test
    public void testTransfer() throws AccountNotExistsException, NotEnoughFundsException {
        bank.transfer(acc2, acc1, 3456);
        long balance1 = bank.getBalance(acc1);
        long balance2 = bank.getBalance(acc2);
        Assert.assertEquals(balance1, 4956);
        Assert.assertEquals(balance2, 8544);
    }

    @Test (expected = NotEnoughFundsException.class)
    public void testTransferWithoutEnoughFounds() throws AccountNotExistsException, NotEnoughFundsException {
        bank.transfer(acc1, acc2, 3456);
    }
}
import java.util.LinkedList;

public class Fifo {
    private LinkedList<String> list;

    public Fifo(){
        list = new LinkedList<String>();
    }

    public synchronized void put(String in) throws InterruptedException {
        System.out.println("put "+Thread.currentThread().getName());
        while (list.size() > 10){
            this.wait();
        }
        list.add(in);
        this.notify();
    }

    public synchronized String get() throws InterruptedException {
        System.out.println("get "+Thread.currentThread().getName());
        String str = "";
        while (list.size() <= 0){
            this.wait();
        }
        str = list.pop();
        this.notify();

        return str;
    }
}

public class Producer implements Runnable {
    private Fifo fifo;
    private String str;
    private int sleeptime;

    public Producer(Fifo f, String s, int n){
        fifo = f;
        str = s;
        sleeptime = n;
    }

    public void go(){
        int i = 0;
        while (true){
            try {
                System.out.println(str+" "+i+" "+System.currentTimeMillis());
                Thread.sleep(1000);
                i++;
            }
            catch (InterruptedException e){
                System.out.println(e);
            }

        }
    }
    public void run() {
        int i = 0;
        while (true){

            try {
                fifo.put(str+i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("produced "+str+" "+i+" "+System.currentTimeMillis());
            i++;
            try {
                Thread.sleep(sleeptime);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
    }
}

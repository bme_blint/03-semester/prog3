public class Consumer implements Runnable{
    private Fifo fifo;
    private String str;
    private int sleeptime;

    public Consumer(Fifo f, String s, int n){
        fifo = f;
        str = s;
        sleeptime = n;
    }

    public void run(){
        while (true){
            try {
                System.out.println("consumed "+str+" "+fifo.get()+" "+System.currentTimeMillis());
                Thread.sleep(sleeptime);
            }
            catch (InterruptedException e){
                System.out.println(e);
            }
        }
    }
}

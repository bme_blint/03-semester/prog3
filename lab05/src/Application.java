public class Application {

    void main(){
        Fifo fifo = new Fifo();

        for (int i = 0; i < 3; ++i){
            Producer prod = new Producer(fifo, "prod"+i, 1000);
            (new Thread(prod)).start();
        }

        for (int i = 0; i < 4; ++i){
            Consumer cons = new Consumer(fifo, "cons"+i, 100);
            (new Thread(cons)).start();
        }
    }
}

package cmds;

import java.io.File;
import java.io.IOException;

public class Ls extends Command {
    public Ls(FileWrapper f) {
        super("ls", f);
    }

    @Override
    public void Func(String[] args) throws IOException {
        File[] flist = getFileWrapper().file.listFiles();
        for (int i = 0; i < flist.length; ++i){
            if (args.length >= 1){
                if (args[0].equals("-l")){
                    if (flist[i].isDirectory()){
                        System.out.println("d - "+flist[i].getName());
                    } else {
                        System.out.println("f - "+flist[i].getName());
                    }
                }
            } else {
                System.out.println(flist[i].getName());
            }
        }
    }
}

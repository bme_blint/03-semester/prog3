package cmds;
import java.io.File;
import java.io.IOException;

public class Command {

    private String name;
    private FileWrapper file;

    public Command(String n, FileWrapper f){
        name = n;
        file = f;
    }
    
    public void Func(String[] args) throws IOException {}

    public FileWrapper getFileWrapper() {
        return file;
    }

    public String getName(){
        return name;
    }
}

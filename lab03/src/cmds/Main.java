package cmds;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) throws IOException {
        
        String wd = System.getProperty("user.dir");
        File f = new File(wd);
        FileWrapper fw = new FileWrapper(f);

        Command[] commands = {new Exit(fw), new Pwd(fw), new Ls(fw), new Cd(fw),
                new Rm(fw), new Mkdir(fw), new Length(fw), new Grep(fw), new Tail(fw)};

        Scanner scanner = new Scanner(System.in);
        
        while (true){
            boolean found = false;
            String string = scanner.nextLine();
            String[] cmd = string.split(" ");
            String command = cmd[0];
            String[] arguments = new String[cmd.length - 1];
            for (int i = 1; i < cmd.length; ++i){
                arguments[i-1] = cmd[i];
            }
            
            for (int i = 0; i < commands.length; ++i){
                if (commands[i].getName().equals(command)){
                    found = true;
                    commands[i].Func(arguments);
                }
            }
            if (!found){
                System.out.println("Command not found, you goose");
            }
        }

    }
}

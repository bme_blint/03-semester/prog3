package cmds;

import java.io.File;
import java.io.IOException;

public class Rm extends Command {
    public Rm(FileWrapper f) {
        super("rm", f);
    }

    @Override
    public void Func(String[] args) throws IOException {
        File removable = new File(getFileWrapper().file, args[0]);
        if (!(removable.delete()))
            System.out.println("Coul'd not remove the file :'(");
    }
}

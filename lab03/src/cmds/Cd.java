package cmds;

import java.io.File;
import java.io.IOException;

public class Cd extends Command {
    public Cd(FileWrapper f) {
        super("cd", f);
    }

    @Override
    public void Func(String[] args) throws IOException {
        if (args[0].equals("..")) {
            getFileWrapper().file = getFileWrapper().file.getParentFile();
        } else {
            if (getFileWrapper().file.isDirectory()){

                getFileWrapper().file = new File(getFileWrapper().file, args[0]);
            } else {
                System.out.println(args[0]+" is not a directory!");
            }
        }
    }
}

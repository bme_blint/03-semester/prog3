package cmds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Tail extends Command {
    public Tail( FileWrapper f) {
        super("tail", f);
    }

    @Override
    public void Func(String[] args) throws IOException {
        if (args.length <= 0){
            System.out.println("usage: tail [-n <n>] <file>");
            return;
        }
        int n = 10;
        int hanyadik = 0;
        if (args.length > 1){
            n = Integer.parseInt(args[1]);
            hanyadik = 2;
        }
        File file = new File(getFileWrapper().file, args[hanyadik]);
        if (file.exists()){
            BufferedReader reader = new BufferedReader(new FileReader(file));
            int i;
            for (i = 0; (reader.readLine()) != null; ++i){}
            reader = new BufferedReader(new FileReader(file));
            for (int j = 0; j < (i-n); ++j){
                reader.readLine();
            }
            String curl;
            while ((curl = reader.readLine()) != null){
                System.out.println(curl);
            }
        } else {
            System.out.println("File does not exist");
        }
    }
}

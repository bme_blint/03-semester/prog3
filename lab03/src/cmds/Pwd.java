package cmds;
import java.io.File;
import java.io.IOException;

public class Pwd extends Command {

    public Pwd(FileWrapper f) {
        super("pwd", f);
    }

    @Override
    public void Func(String[] args) throws IOException {
        System.out.println(getFileWrapper().file.getCanonicalPath());
    }
}

package cmds;

import java.io.File;
import java.io.IOException;

public class Mkdir extends Command {
    public Mkdir( FileWrapper f) {
        super("mkdir", f);
    }

    @Override
    public void Func(String[] args) throws IOException {
        File dir = new File(getFileWrapper().file, args[0]);
        
        File[] flist = getFileWrapper().file.listFiles();
        
        for (File i : flist){
            if (i.getName().equals(args[0])){
                System.out.println("Directory already exist");
                return;
            }
        }
        if (!(dir.mkdir())){
            System.out.println("Directory cannot be created");
        }
    }
}

package cmds;

import java.io.File;
import java.io.IOException;

public class Length extends Command {
    public Length(FileWrapper f) {
        super("length", f);
    }

    @Override
    public void Func(String[] args) throws IOException {
        File lengthable = new File(getFileWrapper().file, args[0]);
        if (lengthable.exists()){
            if (lengthable.isDirectory()){
                System.out.println("Is a directory lel");
            } else {
                System.out.println(lengthable.length());
            }
        } else {
            System.out.println("File does not exist");
        }
    }
}

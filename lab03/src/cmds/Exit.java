package cmds;
import java.io.File;

public class Exit extends Command {

    public Exit(FileWrapper f) {
        super("exit", f);
    }

    @Override
    public void Func(String[] args) {
        System.exit(0);
    }
}

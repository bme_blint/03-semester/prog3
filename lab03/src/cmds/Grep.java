package cmds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Grep extends Command {
    public Grep(FileWrapper f) {
        super("grep", f);
    }

    @Override
    public void Func(String[] args) throws IOException {
        File file = new File(getFileWrapper().file, args[0]);
        
        if (file.exists()){
            String pattern = ".*"+args[0]+".*";
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String curl = "";
            while ((curl = reader.readLine()) != null){
                if (curl.matches(pattern)){
                    System.out.println(curl);
                }
            }
        } else {
            System.out.println("File does not exist");
        }
        
    }
}

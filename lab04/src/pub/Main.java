package pub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static String[] argSplit(String[] cmd){
		String[] arguments = new String[cmd.length - 1];
		if (cmd.length >= 1) System.arraycopy(cmd, 1, arguments, 0, cmd.length - 1);
		return arguments;
	}

    public static void main(String[] args) throws IOException {

		ArrayList<Beer> beers = new ArrayList<>();
		BeerWrapper bw = new BeerWrapper(beers);

		Command[] commands = {new Exit(bw), new Add(bw), new List(bw), new Save(bw), new Load(bw), new Search(bw), new Find(bw), new Delete(bw)};

		Scanner scan = new Scanner(System.in);
		String command = "";

		String user = "\uD83C\uDF7A";
		String host = "pub";
		System.out.print(user+"@"+host+": ~ ");
		while (scan.hasNextLine()) {
			boolean found = false;
			String string = scan.nextLine();
			String[] cmd = string.split(" ");
			command = cmd[0];
			String[] arguments = argSplit(cmd);

			try {
				for (int i = 0; i < commands.length; ++i){
					if (commands[i].getName().equals(command)){
						found = true;
						commands[i].Func(arguments);
					}
				}
				if (!found){
					System.out.println("Command not found, you goose");
				}
			}
			catch (IOException e){
				System.out.println(e);
			}
			System.out.print(user+"@"+host+": ~ ");
 		}
    }
}

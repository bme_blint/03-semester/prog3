package pub;

public class NameComparator implements java.util.Comparator<Beer> {

    @Override
    public int compare(Beer o, Beer t1) {
       return o.getName().compareTo(t1.getName());
    }
}

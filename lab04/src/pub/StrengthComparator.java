package pub;

public class StrengthComparator implements java.util.Comparator<Beer> {

    @Override
    public int compare(Beer o, Beer t1) {
        return Double.compare(o.getStrength(), t1.getStrength());
    }
}

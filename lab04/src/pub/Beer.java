package pub;

import java.io.Serializable;

public class Beer implements Serializable {
    private String name;
    private String style;
    private double strength;

    public Beer (String n, String sty, double str) {
        name = n;
        style = sty;
        strength = str;
    }

    public String toString() {
        return (name+", "+style+", "+strength);
    }

    public String getName() {
        return name;
    }

    public String getStyle() {
        return style;
    }

    public double getStrength() {
        return strength;
    }
}

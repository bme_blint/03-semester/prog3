package pub;

import java.io.IOException;

public class Add extends Command {

    public Add(BeerWrapper bw) {
        super("add", bw);
    }

    @Override
    public void Func(String[] args) throws IOException {
        if (args.length < 3){
            System.out.println("usage: add <name> <style> <strength>");
            return;
        }
        Beer beer = new Beer(args[0], args[1], Double.parseDouble(args[2]));
        if(!getBeerWrapper().beers.add(beer)){
            System.out.println("Could not add beer to the list");
        }
    }
}

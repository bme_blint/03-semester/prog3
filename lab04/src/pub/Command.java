package pub;

import java.io.IOException;
import java.util.ArrayList;

public abstract class Command {

    private String name;
    private BeerWrapper beerWrapper;

    public Command(String n, BeerWrapper bw){
        name = n;
        beerWrapper = bw;
    }

    public void Func(String[] args) throws IOException {}

    public String getName(){

        return name;
    }
    public BeerWrapper getBeerWrapper() {
        return beerWrapper;
    }

    public void setBeerWrapper (ArrayList<Beer> b) {
        beerWrapper.beers = b;
    }
}


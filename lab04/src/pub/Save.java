package pub;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Save extends Command {
    public Save( BeerWrapper bw) {
        super("save", bw);
    }

    @Override
    public void Func(String[] args) throws IOException {
        if (args.length < 1){
            System.out.println("usage error"); //TODO: write normal error message!!!
            return;
        }
        try {
            FileOutputStream f = new FileOutputStream(args[0]);
            ObjectOutputStream out = new ObjectOutputStream(f);
            out.writeObject(getBeerWrapper().beers);
            out.close();
        }
        catch (IOException ex){
           System.out.println(ex);
        }
    }
}

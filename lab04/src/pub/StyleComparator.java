package pub;

public class StyleComparator implements java.util.Comparator<Beer> {

    @Override
    public int compare(Beer o, Beer t1) {
        return o.getStyle().compareTo(t1.getStyle());
    }
}

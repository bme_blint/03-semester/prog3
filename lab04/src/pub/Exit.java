package pub;

import java.io.IOException;

public class Exit extends Command {
    public Exit(BeerWrapper beerWrapper) {
        super("exit", beerWrapper);
    }

    @Override
    public void Func(String[] args) {
        System.exit(0);
    }
}

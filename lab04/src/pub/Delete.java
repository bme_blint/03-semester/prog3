package pub;

import java.io.IOException;
import java.util.Iterator;

public class Delete extends Command {
    public Delete( BeerWrapper bw) {
        super("delete", bw);
    }

    @Override
    public void Func(String[] args) throws IOException {
        if (args.length <= 0){
            System.out.println("bad usage");
            return;
        }
        Iterator<Beer> iter = getBeerWrapper().beers.iterator();
        while (iter.hasNext()){
            if (iter.next().getName().equals(args[0])){
                iter.remove();
            }
        }
    }
}

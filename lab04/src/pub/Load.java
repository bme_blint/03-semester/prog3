package pub;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class Load extends Command {
    public Load(BeerWrapper bw) {
        super("load", bw);
    }

    @Override
    public void Func(String[] args) throws IOException {
        if (args.length < 1){
            System.out.println("usage error"); //TODO: write normal error message!!!
            return;
        }
        try {
            FileInputStream f = new FileInputStream(args[0]);
            ObjectInputStream in = new ObjectInputStream(f);
            setBeerWrapper((ArrayList<Beer>)in.readObject());
            in.close();
        }
        catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex);
        }
    }
}

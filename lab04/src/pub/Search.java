package pub;

import java.io.IOException;

public class Search extends Command{
    public Search( BeerWrapper bw) {
        super("search", bw);
    }

    @Override
    public void Func(String[] args) throws IOException {
        if (args.length <= 0) {
            System.out.println("bad usage");
            return;
        }
        for (Beer b: getBeerWrapper().beers) {
            if (args.length == 2){
                if (args[0].equals("name")){
                    if (b.getName().equals(args[1])){
                        System.out.println(b.toString());
                    }
                } else if (args[0].equals("style")){
                    if (b.getStyle().equals(args[1])){
                        System.out.println(b.toString());
                    }
                } else if (args[0].equals("strength")){
                    if (b.getStrength() == Double.parseDouble(args[1])){
                        System.out.println(b.toString());
                    }
                } else {
                    return;
                }

            } else if (args.length == 1) {
                if (b.getName().equals(args[0])){
                    System.out.println(b.toString());
                }
            }
        }
    }
}

package pub;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

public class List extends Command{
    public List( BeerWrapper bw) {
        super("list", bw);
    }

    @Override
    public void Func(String[] args) throws IOException {

        if (args.length == 1) {
            Comparator<Beer> comp;
            if (args[0].equals("name")){
                comp = new NameComparator();
            } else if (args[0].equals("style")){
                comp = new StyleComparator();
            } else if (args[0].equals("strength")){
                comp = new StrengthComparator();
            } else {
                System.out.println("bad arguments");
                return;
            }

            Collections.sort(getBeerWrapper().beers, comp);
        }

        for (Beer b: getBeerWrapper().beers) {
            System.out.println(b.toString());
        }


    }
}
